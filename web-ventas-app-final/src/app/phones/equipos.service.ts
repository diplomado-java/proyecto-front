import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EquiposService {

  public readonly HEADER_JSON =
    new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });

  constructor(protected http: HttpClient) {
  }

  // TODO
  // obtenerEquipos(): Observable<any> {
  //   return this.http.get<>('http://localhost:8080/api/v1/employee/by/identity/1065', {headers: this.HEADER_JSON});
  // }
}

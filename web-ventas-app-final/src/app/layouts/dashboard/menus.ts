export interface Menu {
  link: string;
  texto: string;
  icon: string;
  divisor: boolean;
  linkAbsoluto: string;
  permission?: string;
}

export class Menus {

  public static readonly LINK_INICIO: string = 'inicio';
  public static readonly LINK_EMPLEADOS: string = 'inicio';

  public static readonly MENU_INICIO: Menu = {
    link: Menus.LINK_INICIO,
    texto: 'Inicio',
    divisor: false,
    icon: 'home',
    linkAbsoluto: '',
  };

  public static readonly MENU_EMPLEADOS: Menu = {
    link: Menus.LINK_EMPLEADOS,
    texto: 'Empleados',
    divisor: false,
    icon: 'shop',
    linkAbsoluto: '',
  };

  public static readonly LINK_CELULARES: string = 'celulares';

  public static readonly MENU_CELULARES: Menu = {
    link: Menus.LINK_CELULARES,
    texto: 'Celulares',
    divisor: false,
    icon: 'setting',
    linkAbsoluto: '',
  };

}

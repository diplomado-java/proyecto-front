import {Component, OnInit} from '@angular/core';
import {Menu} from './menus';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public menus: Menu[] = [];
  public isCollapsed = false;
  public active: string = '';

  constructor(
    // private router: Router,
  ) {
  }

  ngOnInit(): void {
  }

}

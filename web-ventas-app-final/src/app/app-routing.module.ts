import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EmployeesComponent} from './employees/employees.component';
import {EquiposMovilesComponent} from './phones/equipos-moviles.component';
import {FormEmployeeComponent} from './employees/form-employee/form-employee.component';

const routes: Routes = [
  {path: "empleados", component: EmployeesComponent},
  {path: "celulares", component: EquiposMovilesComponent},
  {path: "crearEmpleado", component: FormEmployeeComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    // CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

import {Component, OnInit} from '@angular/core';
import {EmployeesService} from './employees.service';
import {Employee} from '../models/employee';
import {Router} from '@angular/router';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees: Employee[] = [];

  constructor(private employeeService: EmployeesService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.llenarEmpleados();
    this.getEmployee();
  }

  llenarEmpleados() {
    // @ts-ignore
    let employee: Employee = new Employee();
    employee.employeeCode = '122';
    employee.name = 'Juan';
    employee.identity = '12121212';
    this.employees.push(employee);
  }

  getEmployee(): void {
    this.employeeService.getEmployee(1).subscribe((respuesta) => {
        respuesta.data;
      }
    );
  }

  createNewEmployee() {
    this.router.navigateByUrl('/crearEmpleado')
  }

}

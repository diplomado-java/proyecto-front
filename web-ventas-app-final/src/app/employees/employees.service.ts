import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RespuestaAPI} from '../models/respuestaAPI';
import {Employee} from '../models/employee';

@Injectable()
export class EmployeesService {

  public readonly HEADER_JSON =
    new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });

  constructor(
    private http: HttpClient
  ) { }

  getEmployee(id: number): Observable<any> {
    return this.http.get<RespuestaAPI<Employee>>(`http://localhost:8080/api/v1/employee/by/identity/${id}`, {headers: this.HEADER_JSON});
  }

  getEmployeesList(id: number): Observable<any> {
    return this.http.get<RespuestaAPI<Employee[]>>(`http://localhost:8080/api/v1/employee/`, {headers: this.HEADER_JSON});
  }
}

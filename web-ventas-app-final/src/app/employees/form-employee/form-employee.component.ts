import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Employee} from '../../models/employee';

@Component({
  selector: 'app-form-employee',
  templateUrl: './form-employee.component.html',
  styleUrls: ['./form-employee.component.css']
})
export class FormEmployeeComponent implements OnInit {

  // @ts-ignore
  public form: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.configurarFormulario();
  }

  private configurarFormulario(): void {
    this.form = new FormGroup({
      nombre: new FormControl(),
      numeroIdent: new FormControl(),
      codigo: new FormControl()
    });
  }

  onSubmit() {

  }

  crearEmpleado() {
    // @ts-ignore
    let newEmployee: Employee = new Employee();
    newEmployee.name = this.form.get('nombre')?.value;
    newEmployee.identity = this.form.get('numeroIdent')?.value;
    newEmployee.employeeCode = this.form.get('codigo')?.value;
  }
}

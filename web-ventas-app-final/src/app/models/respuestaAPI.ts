export class RespuestaAPI <T> {
  constructor(
    public status: string,
    public message: string,
    public timestamp: string,
    // tslint:disable-next-line:variable-name
    public data: T,
  ) {
  }
}

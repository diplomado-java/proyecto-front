import {TypeEmployeeEntity} from './typeEmployeeEntity';

export class Employee {
  constructor(
    public identity: string = '',
    public name: string = '',
    public employeeCode: string = '',
    public type: TypeEmployeeEntity
  ) {
  }
}

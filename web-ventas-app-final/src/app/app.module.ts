import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {DashboardComponent} from './layouts/dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import {EmployeesComponent} from './employees/employees.component';
import {EquiposMovilesComponent} from './phones/equipos-moviles.component';
import {FormEmployeeComponent} from './employees/form-employee/form-employee.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    EmployeesComponent,
    EquiposMovilesComponent,
    FormEmployeeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
